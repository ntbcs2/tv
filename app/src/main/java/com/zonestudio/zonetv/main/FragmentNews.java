package com.zonestudio.zonetv.main;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.zonestudio.zonetv.R;
import com.zonestudio.zonetv.model.Calendar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ntb on 1/29/2015.
 */
public class FragmentNews extends Fragment implements View.OnClickListener {

    private WebView wvCalendar;
    private List<Calendar> listCalendar;
    private List<String> listURL;
    private List<String> listTitle;
    private Spinner spListChanel;
    private Button btSubmit;
    private ProgressDialog dialog;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_news, null);
        spListChanel = (Spinner) view.findViewById(R.id.splistChanel);
        btSubmit = (Button) view.findViewById(R.id.btSubmit);

        btSubmit.setOnClickListener(this);
        getData();

        return view;
    }

    private void getData() {


        listCalendar = new ArrayList<>();
        listTitle = new ArrayList<>();
        listURL = new ArrayList<>();

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                Document doc = null;
                try {

                    doc = Jsoup.connect("http://tv.vietbao.vn/lich-phat-song/vtv2/").
                            userAgent("Mozilla").
                            get();

                    Elements element = doc.select("select#selectChannel").select("optgroup");
                    for (Element e : element) {

                        Elements el = e.select("option");
                        for (Element e1 : el) {

                            Calendar calendar = new Calendar();
                            calendar.setTitle(e1.text());
                            calendar.setUrl(e1.attr("value"));
                            listURL.add(e1.attr("value"));
                            listTitle.add(e1.text());

                        }

                    }


                } catch (IOException ex) {

                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                spListChanel.setAdapter(new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, listTitle));
            }
        }.execute();
    }

    @Override
    public void onClick(View v) {
        new GetData().execute(listURL.get(spListChanel.getSelectedItemPosition()));
    }

    private class GetData extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(getActivity());
            dialog.show();

            wvCalendar = (WebView) view.findViewById(R.id.wvCalendar);
            wvCalendar.getSettings().setJavaScriptEnabled(true);
            wvCalendar.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            wvCalendar.setWebChromeClient(new WebChromeClient() {
            });
            wvCalendar.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return true;
                }
            });
        }

        @Override
        protected String doInBackground(String... params) {

            Document doc = null;
            try {
                doc = Jsoup.connect(params[0]).
                        userAgent("Mozilla").
                        get();

                Elements element = doc.select("div#container").
                        select("div#contentadd").
                        select("div#ct").
                        select("div#inner-left").
                        select("div.tivi-detail");
                Elements el = element.select("div[class=htv-channel clearfix] a");
                el.remove();
                Elements el1 = element.select("div[class=weekdays clearfix]");
                el1.remove();
                Elements el2 = element.select("div[class=box_note]");
                el2.remove();
                Elements el3 = element.select("div[class=chanel-detail] iframe");
                el3.remove();
                Elements el4 = element.select("img");
                el4.remove();

                return element.html();

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            String css = ".chanel-detail {\n" +
                    "\t\t\tborder: 1px solid #e7e7e7;\n" +
                    "\t\t\tborder-top: none;\n" +
                    "\t\t\tmargin-bottom: 5px;\n" +
                    "\t\t}\n" +
                    "\n" +
                    "\t\t.title-tv, .chanel-detail h3 {\n" +
                    "\t\t\tbackground: url(http://vietbao.vn/images/v2011/sprice_bgadd.gif) repeat-x left -274px;\n" +
                    "\t\t\tline-height: 24px;\n" +
                    "\t\t\theight: 24px;\n" +
                    "\t\t\toverflow: hidden;\n" +
                    "\t\t\tborder-bottom: 1px solid #e7e7e7;\n" +
                    "\t\t\tborder-top: 1px solid #e7e7e7;\n" +
                    "\t\t\tcolor: #000;\n" +
                    "\t\t\tpadding: 0px 0px 0px 10px;\n" +
                    "\t\t\tfont-size: 14px;\n" +
                    "\t\t\tfont-weight: bold;\n" +
                    "\t\t}\n" +
                    "\t\thtml[xmlns] .clearfix {\n" +
                    "\t\t\tdisplay: block;\n" +
                    "\t\t}\n" +
                    "\t\t.tivi-detail .tv-detail {\n" +
                    "\t\t\tposition: relative;\n" +
                    "\t\t}\n" +
                    "\t\t.tv-detail {\n" +
                    "\t\t\tpadding: 5px;\n" +
                    "\t\t\tline-height: 20px;\n" +
                    "\t\t}\n" +
                    "\t\t.time-tv {\n" +
                    "\t\t\tfloat: left;\n" +
                    "\t\t\twidth: 108px;\n" +
                    "\t\t\ttext-align: center;\n" +
                    "\t\t\tfont-weight: bold;\n" +
                    "\t\t\tpadding: 3px 3px 5px;\n" +
                    "\t\t}\n" +
                    "\t\ta {\n" +
                    "\t\t\tcolor: #005399;\n" +
                    "\t\t\ttext-decoration: none;\n" +
                    "\t\t\tfont-family: Arial, Helvetica, sans-serif;\n" +
                    "\t\t}\n" +
                    "\t\tspan, .t-detail a {\n" +
                    "\t\t\tcolor: #e05a13;\n" +
                    "\t\t\tfont-weight: bold;\n" +
                    "\t\t}" +
                    ".t-detail p {\n" +
                    "\t\t\tdisplay: block;\n" +
                    "\t\t\tposition:relative;\n" +
                    "\t\t\tleft:116px;\n" +
                    "\t\t\n" +
                    "font-size:15px;\n" +
                    "\t\t\tcolor:#294C74;" +
                    "width:200px" +
                    "\t\t}";

            String html = "<html>\n" +
                    "<head>\n" +
                    "\t<meta charset=\"utf-8\">\n" +
                    "\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                    "\t<title></title>\n" +
                    "\t<link rel=\"stylesheet\" href=\"\">\n" +
                    "\t<style type=\"text/css\" media=\"screen\">\n" +
                    "\t\t\n" + css +
                    "\t</style>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\t\n" + aVoid +
                    "</body>\n" +
                    "</html>";
            wvCalendar.loadData(html, "text/html;charset=UTF-8", null);
            dialog.dismiss();
        }

    }
}
