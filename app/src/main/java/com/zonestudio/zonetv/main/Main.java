package com.zonestudio.zonetv.main;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.View;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.zonestudio.zonetv.R;
import com.zonestudio.zonetv.appcontroller.AppController;
import com.zonestudio.zonetv.instant.Common_Instant;
import com.zonestudio.zonetv.model.Chanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.liveo.interfaces.NavigationLiveoListener;
import br.liveo.navigationliveo.NavigationLiveo;
import io.vov.vitamio.LibsChecker;

/**
 * Created by ntb on 1/28/2015.
 */
public class Main extends NavigationLiveo implements NavigationLiveoListener, Common_Instant {

    private List<String> listNameItem;
    private Fragment fragment;

    @Override
    public void onUserInformation() {

        this.mUserName.setText("VietTV - Tivi Trực Tuyến Cho Người Việt");
        this.mUserEmail.setText("http://zonesoft.net/");
        this.mUserEmail.setPadding(20,20,20,20);
        this.mUserEmail.setTextSize(12);
        this.mUserEmail.setTextColor(Color.parseColor("#69a63f"));
        this.mUserPhoto.setImageResource(R.mipmap.logo);
        this.mUserPhoto.setAdjustViewBounds(true);
        this.mUserBackground.setBackgroundColor(Color.parseColor("#69a63f"));
    }


    @Override
    public void onInt(Bundle savedInstanceState) {

        this.setColorSelectedItemNavigation(R.color.actionbar);
        this.setNavigationListener(this);
        this.setDefaultStartPositionNavigation(1);
        listNameItem = new ArrayList<>();
        listNameItem.add(0, "Kênh");
        listNameItem.add(1, "Tivi Trực Tuyến");
        listNameItem.add(2, "Khác");
        listNameItem.add(3, "Kênh Yêu Thích");
        listNameItem.add(4, "Lịch Sử");
        listNameItem.add(5, "Chia Sẻ");
        listNameItem.add(6, "Cài Đặt");


        List<Integer> listIconItem = new ArrayList<>();
        listIconItem.add(0,0);
        listIconItem.add(1,R.mipmap.ic_tv_grey600_18dp);
        listIconItem.add(2,0);
        listIconItem.add(3,R.mipmap.ic_favorite_grey600_18dp);
        listIconItem.add(4,R.mipmap.ic_history_grey600_36dp);
        listIconItem.add(5,R.mipmap.ic_share_grey600_24dp);
        listIconItem.add(6,R.mipmap.ic_settings_grey600_36dp);

        List<Integer> listHeader = new ArrayList<>();
        listHeader.add(2);
        listHeader.add(0);

        SparseIntArray mSparseCounterItem = new SparseIntArray();
        mSparseCounterItem.put(1, 64);

        this.setNavigationAdapter(listNameItem,listIconItem,listHeader,mSparseCounterItem);

    }

    @Override
    public void onItemClickNavigation(int position, int layoutContainerId) {

        switch (position){
            case 1:
                fragment = new FragmentMain();
                break;
           default:
               Log.d("Error","");
               break;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(layoutContainerId,fragment).commit();
    }

    @Override
    public void onPrepareOptionsMenuNavigation(Menu menu, int position, boolean visible) {

    }

    @Override
    public void onClickFooterItemNavigation(View v) {

    }

    @Override
    public void onClickUserPhotoNavigation(View v) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://zonesoft.net/"));
        startActivity(intent);
    }
}
