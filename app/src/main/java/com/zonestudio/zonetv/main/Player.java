package com.zonestudio.zonetv.main;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.zonestudio.zonetv.R;
import com.zonestudio.zonetv.model.Chanel;

import java.io.IOException;
import java.util.List;

import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;

public class Player extends Activity implements  SurfaceHolder.Callback,SeekBar.OnSeekBarChangeListener, View.OnClickListener {


    private MediaPlayer mMediaPlayer;
    private SurfaceView mPreview;
    private SurfaceHolder holder;
    private ProgressBar pb;
    private TextView downloadRateView, loadRateView;
    private List<String> listChanel;
    private TextView tvTimeStart;
    private TextView tvTimeEnd;
    private ImageView imgPlay;
    private SeekBar seekBarVolumn;
    private AudioManager audioManager;
    private Bundle extras;
    private ImageView imgZoom;
    private ShowcaseView showcaseView;
    private int mVideoWidth;
    private int mVideoHeight;
    private boolean mIsVideoSizeKnown = false;
    private boolean mIsVideoReadyToBePlayed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!LibsChecker.checkVitamioLibs(this))
            return;
        setContentView(R.layout.layout_play);
        mPreview = (SurfaceView) findViewById(R.id.surface);
        holder = mPreview.getHolder();
        holder.addCallback(this);
        holder.setFormat(PixelFormat.RGBA_8888);
        extras = getIntent().getExtras();
     //   initialWidGet();

    }


    private void initialWidGet() {

        Intent intent = getIntent();
        Chanel chanel = (Chanel) intent.getSerializableExtra("chanel");

        tvTimeEnd = (TextView) findViewById(R.id.tvTimeEnd);
        tvTimeStart = (TextView) findViewById(R.id.tvTimeStart);
        downloadRateView = (TextView) findViewById(R.id.download_rate);
        loadRateView = (TextView) findViewById(R.id.load_rate);
        seekBarVolumn = (SeekBar) findViewById(R.id.seekBarVolumn);
        imgZoom = (ImageView) findViewById(R.id.imgZoom);
        imgZoom.setOnClickListener(this);
        seekBarVolumn.setOnSeekBarChangeListener(this);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        seekBarVolumn.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekBarVolumn.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));


        listChanel = chanel.getStream_url();
        pb = (ProgressBar) findViewById(R.id.probar);

        mMediaPlayer = new MediaPlayer(this);
        try {

            mMediaPlayer.setDataSource(listChanel.get(1));

        } catch (IOException e) {

            e.printStackTrace();
        }
        mMediaPlayer.setDisplay(holder);
        mMediaPlayer.prepareAsync();

        mMediaPlayer.start();
        ViewTarget target = new ViewTarget(R.id.imgZoom, this);
        showcaseView = new ShowcaseView.Builder(this, true).
                setTarget(target).
                setContentTitle("Hello").
                setContentText("Okies").
                build();
        showcaseView.show();
    }

  /*  @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        loadRateView.setText(percent + "%");
    }*/

   /* @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                if (mVideoView.isPlaying()) {
                    mVideoView.pause();
                    pb.setVisibility(View.VISIBLE);
                    downloadRateView.setText("");
                    loadRateView.setText("");
                    downloadRateView.setVisibility(View.VISIBLE);
                    loadRateView.setVisibility(View.VISIBLE);
                    tvTimeEnd.setText(String.valueOf(mVideoView.getDuration()));

                }
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                mVideoView.start();
                pb.setVisibility(View.GONE);
                downloadRateView.setVisibility(View.GONE);
                loadRateView.setVisibility(View.GONE);
                break;
            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
                downloadRateView.setText("" + extra + "kb/s" + "  ");
                break;

        }
        return true;
    }*/

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgZoom:
               /* mVideoView.setVideoLayout(VideoView.VIDEO_LAYOUT_ZOOM, 0);
                mVideoView.pause();*/
                break;
        }
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        initialWidGet();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
