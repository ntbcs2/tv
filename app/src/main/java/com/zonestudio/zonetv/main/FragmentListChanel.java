package com.zonestudio.zonetv.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.zonestudio.zonetv.R;
import com.zonestudio.zonetv.adapter.GridView_Chanel_Adapter;
import com.zonestudio.zonetv.appcontroller.AppController;
import com.zonestudio.zonetv.instant.Common_Instant;
import com.zonestudio.zonetv.model.Chanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ntb on 1/29/2015.
 */
public class FragmentListChanel extends Fragment implements Common_Instant {

    private GridView gdvListChanel;
    private List<Chanel> listChanel;
    private GridView_Chanel_Adapter adapter;
    private static final String TAG = FragmentListChanel.class.getSimpleName();
    private ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_chanel, null);
        initial(view);
        return view;
    }

    private void initial(View view) {

        listChanel = new ArrayList<>();
        getListChanel();
        gdvListChanel = (GridView) view.findViewById(R.id.grdListChanel);
        adapter = new GridView_Chanel_Adapter(getActivity(), listChanel);
        gdvListChanel.setAdapter(adapter);
        gdvListChanel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Chanel chanel = (Chanel) listChanel.get(position);
                Intent intent = new Intent(getActivity(), Player.class);
                intent.putExtra("chanel",chanel);
                startActivity(intent);
            }
        });

    }

    private void getListChanel() {

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("loading...");
        dialog.show();

        JsonArrayRequest jsonArray = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {

                            try {

                                Chanel chanel = new Chanel();

                                JSONObject jsonObject = (JSONObject) response
                                        .get(i);

                                chanel.setTitle(jsonObject.getString("title"));
                                chanel.setLogo(replaceString(jsonObject.getString("tcon")));

                                JSONObject jsonStreamURl = jsonObject
                                        .getJSONObject("url");

                                List<String> listStream = new ArrayList<String>();

                                listStream.add(replaceString(jsonStreamURl.getString("low_quality")));
                                listStream.add(replaceString(jsonStreamURl.getString("normal_quality")));
                                listStream.add(replaceString(jsonStreamURl.getString("high_quality")));
                                Log.d(TAG, replaceString(jsonStreamURl.getString("high_quality")));

                                chanel.setStream_url(listStream);


                                listChanel.add(chanel);

                            } catch (JSONException ex) {

                                Log.d(TAG, ex.getMessage(), ex);
                            }

                        }

                        adapter.notifyDataSetChanged();
                        dialog.dismiss();

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, error.getMessage(), error);
            }

        });

        jsonArray.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonArray);

    }

    private String replaceString(String url) {

        return url.replace("\\", "/");
    }
}
