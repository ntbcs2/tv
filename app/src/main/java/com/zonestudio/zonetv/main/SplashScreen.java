package com.zonestudio.zonetv.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.zonestudio.zonetv.R;

/**
 * Created by ntb on 2/5/2015.
 */
public class SplashScreen extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();
                Intent intent = new Intent(SplashScreen.this, Main.class);
                startActivity(intent);
            }
        }, 5000);
    }
}
