package com.zonestudio.zonetv.model;

/**
 * Created by ntb on 2/4/2015.
 */
public class Calendar {

    private String title;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
