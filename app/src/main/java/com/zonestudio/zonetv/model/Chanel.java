package com.zonestudio.zonetv.model;

import java.io.Serializable;
import java.util.List;

public class Chanel implements Serializable {

	private String logo;
	private List<String> stream_url;
	private String title;

	public Chanel() {
	}

	public Chanel(String logo, List<String> stream_url, String title) {

		this.logo = logo;
		this.stream_url = stream_url;
		this.title = title;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public List<String> getStream_url() {
		return stream_url;
	}

	public void setStream_url(List<String> stream_url) {
		this.stream_url = stream_url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
