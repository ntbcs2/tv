package com.zonestudio.zonetv.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.zonestudio.zonetv.R;
import com.zonestudio.zonetv.appcontroller.AppController;
import com.zonestudio.zonetv.model.Chanel;


public class GridView_Chanel_Adapter extends BaseAdapter {

	private List<Chanel> listChanel;
	private Context context;
	private LayoutInflater inflater;
	private ImageLoader imageLoader;
	
	public GridView_Chanel_Adapter(Context context, List<Chanel> listChanel) {
	
		this.context = context;
		this.listChanel = listChanel;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = AppController.getInstance().getImageLoader();
	}
	
	
	@Override
	public int getCount() {
		return listChanel.size();
	}

	@Override
	public Object getItem(int position) {
		return listChanel.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(convertView == null){
			
			convertView = inflater.inflate(R.layout.custom_gridview_main,null);
		}
		
		NetworkImageView imageLogo = (NetworkImageView)convertView.findViewById(R.id.imgLogo);
		imageLogo.setImageUrl(listChanel.get(position).getLogo(), imageLoader);
		
		return convertView;
	}

	
}
